import { connectToDatabase } from "../lib/database";
const { ObjectId } = require("mongodb");
import { allowCors } from "../lib/cors"; // 解决跨域

const handle = async (req, res) => {
	const db = await connectToDatabase();
	const collection = await db.collection("articles");
	let articles, article, result;
	let id = req.query.id;
	switch (req.method) {
		case "GET":
			if (!id) {
				// 这里是直接get根目录
				articles = await collection.find({}).toArray();
				res.status(200).json(articles);
			}
			// get 某一篇文章
			else {
				article = await collection.findOne({
					_id: ObjectId(String(id)),
				});
				res.status(200).json(article);
			}
			break;
		case "POST":
			let newpost = req.body;
			articles = await collection.insertOne(newpost);
			res.status(200).json({ users: articles });
			break;
		case "PATCH":
			result = await collection.findOneAndUpdate(
				{ _id: ObjectId(String(id)) },
				{ $set: req.body }
			);
			res.status(200).json(result);
			break;
		case "DELETE":
			result = await collection.deleteOne({
				_id: ObjectId(String(id)),
			});
			res.status(200).json(result);
			break;
		case "PUT": // 迷惑的是网页的点赞会失败 postman就可以
			article = await collection.findOne({
				_id: ObjectId(String(id)),
			});
			result = await collection.findOneAndUpdate(
				{ _id: ObjectId(String(id)) },
				{ $set: { up: article.up + 1 } }
			);
			res.status(200).json(result);
			break;
		default:
			res.status(404).json({ status: "ERROR ROUTE NOT FOUND" });
	}
};

module.exports = allowCors(handle);
