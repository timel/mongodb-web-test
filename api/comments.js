import { connectToDatabase } from "../lib/database";
const { ObjectId } = require("mongodb");
import { allowCors } from "../lib/cors"; // 解决跨域

const handle = async (req, res) => {
	const db = await connectToDatabase();
	const collection = await db.collection("comments");
	let comments, result;
	let article_id = req.query.article_id;
	let comment_id = req.query.comment_id;
	switch (req.method) {
		case "GET":
			comments = await collection
				.find({
					article_id: article_id,
				})
				.toArray();
			res.status(200).json(comments);
			break;
		case "POST":
			let newcomment = req.body;
			result = await collection.insertOne(newcomment);
			res.status(200).json(result);
			break;
		case "DELETE":
			result = await collection.deleteOne({
				_id: ObjectId(String(comment_id)),
			});
			res.status(200).json(result);
			break;
		default:
			res.status(404).json({ status: "ERROR ROUTE NOT FOUND" });
	}
};

module.exports = allowCors(handle);
