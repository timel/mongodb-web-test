const MongoClient = require("mongodb").MongoClient;
let cachedDb = null;
const DATABASENAME = "vercel_test";
const url = "mongodb+srv://admin:mypassword@cluster0.mejvq.mongodb.net/test";
export const connectToDatabase = async () => {
	if (cachedDb) {
		console.log("Use existing connection");
		return Promise.resolve(cachedDb);
	} else {
		return MongoClient.connect(url, {
			useUnifiedTopology: true,
		})
			.then((client) => {
				let db = client.db(DATABASENAME);
				console.log("New Database connection");
				cachedDb = db;
				return cachedDb;
			})
			.catch((error) => {
				console.log("Mongo connection error");
				console.error(error);
			});
	}
};
